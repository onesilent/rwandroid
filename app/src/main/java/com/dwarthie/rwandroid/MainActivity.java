package com.dwarthie.rwandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button write;
    Button read;
    EditText fileName;
    EditText text;
    TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String LOG = "debug";

        write = (Button) findViewById(R.id.write);
        read = (Button) findViewById(R.id.read);
        fileName = (EditText) findViewById(R.id.name);
        text = (EditText) findViewById(R.id.text);
        res = (TextView) findViewById(R.id.res);


        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Log.d(LOG, "NAME:"+fileName.getText().toString() + "\nTEXT:" + text.getText().toString());
                    write_string(fileName.getText().toString(), text.getText().toString());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    res.clearComposingText();
                    res.append(read_file(fileName.getText().toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        /*try {
            write_string("asd", "asd");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void write_string (String fileName, String str) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(openFileOutput(fileName, MODE_PRIVATE)));
        bw.write(str);
        bw.close();
    }

    public String read_file (String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput(fileName)));
        String res = "";
        String temp = "";
        while ((temp = br.readLine()) != null) {
            res += temp;
            temp = null;
        }

        return res;
    }
}
